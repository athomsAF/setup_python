import nox

PYTHON_VERSIONS = ["3.11"]
env: str = "virtualenv"


@nox.session(
    venv_backend=env,
    python=PYTHON_VERSIONS,
)  # use this annotation on the wrapper works like a charm
def format(session: nox.Session) -> None:  # noqa: A001
    """
    Format the code.

    Args:
    ----
        session (nox.Session): _description_

    """
    session.install("-r", "requirements/requirements-format.txt")


@nox.session(venv_backend=env, python=PYTHON_VERSIONS)
def dev(session: nox.Session) -> None:
    """
    Create a dev session.

    Args:
    ----
        session (nox.Session): _description_

    """
    session.install("-r", "requirements/requirements-nox.txt")
    session.install("-r", "requirements/requirements-format.txt")
    session.install("-r", "requirements/requirements-doc.txt")
    session.install("-r", "requirements/requirements-test.txt")
    session.install("-r", "requirements.txt")
    session.run("pre-commit", "install")


@nox.session(venv_backend=env, python=PYTHON_VERSIONS)
def test(session: nox.Session) -> None:
    """
    Create a test session.

    Args:
    ----
        session (nox.Session): _description_

    """
    session.install("-r", "requirements/requirements-test.txt")
    session.install("-r", "requirements/requirements-nox.txt")
    session.install("-r", "requirements.txt")


@nox.session(venv_backend=env, python=PYTHON_VERSIONS)
def docs(session: nox.Session) -> None:
    """
    Create a docs session.

    Args:
    ----
        session (nox.Session): _description_

    """
    session.install("-r", "requirements/requirements-doc.txt")


# @nox.session(venv_backend=env, python=PYTHON_VERSIONS)
# def build(session: nox.Session) -> None:
#     """
#     Create a build session.
#
#     Args:
#     ----
#         session (nox.Session): _description_
#
#     """
#     session.install("poetry")
#     session.run("poetry", "install", "--without", "dev")
